#include "BNO055.h"

#include <avr/io.h>
#include <util/delay.h>
#include <util/twi.h>

// The breakout board pulls COM3_state low internally
#define DEVICE_ADDR 0x28

// Registers
#define UNIT_SELECT_ADDR 0x3B
#define OPER_MODE_ADDR 0x3D
#define HEADING_MSB_ADDR 0x1B
#define HEADING_LSB_ADDR 0x1A

// Values
#define COMPASS_MODE 0x09
#define DEGREE_MODE 0x06

#define F_CPU 16000000UL
#define SCL_CLOCK 400000L

inline uint8_t get_status(void) {
    return TWSR & 0xF8;
}

inline bool start(void) {
	TWCR = (1 << TWSTA) | (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));
	return get_status() == TW_START;
}

inline void stop(void) {
	TWCR = (1 << TWSTO) | (1 << TWINT) | (1 << TWEN);
	_delay_ms(1);
}

bool send_address(uint8_t w) {
	TWDR = (DEVICE_ADDR<<1) | w ;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	const uint8_t status = get_status();
	return status == TW_MT_SLA_ACK || status == TW_MR_SLA_ACK;
}

uint8_t read(void) {
    TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));
	return TWDR;
}

bool write(uint8_t data) {
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));
	return get_status() == TW_MT_DATA_ACK;
}

uint8_t read_address(uint8_t addr) {
	if(!start()) return 0xFF;
    if(!send_address(TW_WRITE)) return 0xFE;
    if(!write(addr)) return get_status();
	stop();

	if(!start()) return 0xFC;
    if(!send_address(TW_READ)) return get_status();
    const uint8_t value = read();
	stop();

	return value;
}

bool write_address(uint8_t addr, uint8_t value) {
	if(!start()) return false;
    if(!send_address(TW_WRITE)) return false;
    if(!write(addr)) return false;
    if(!write(value)) return false;
	stop();
	return true;
}

bool init_sensor(void) {
    // Setup I2C
    DDRD &= ~((1 << 0) | (1 << 1));
    PORTD |= ((1 << 0) | (1 << 1));

	TWBR = ((( F_CPU / SCL_CLOCK ) - 16) / 2);
	TWSR = 0;
	TWCR = ((1 << TWEN) | (1 << TWIE) | (1 << TWEA));

	// Set UNIT_SELECT
	if(!write_address(UNIT_SELECT_ADDR, DEGREE_MODE)) return false;

    // Set OPER_MODE
    if(!write_address(OPER_MODE_ADDR, COMPASS_MODE)) return false;

	return true;
}

uint16_t read_heading(void) {
    uint16_t heading = read_address(0x1A) | (read_address(0x1B) << 8);
    heading = (uint16_t)(heading / 16.0);
    return heading;
}
