#ifndef _BNO055_H_
#define _BNO055_H_

#include <stdbool.h>
#include <stdint.h>

bool init_sensor(void);
uint8_t read_address(uint8_t address);
uint16_t read_heading(void);

#endif // _BNO055_H_
