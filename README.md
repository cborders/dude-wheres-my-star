# Dude, where's my star? #

This repo contains the firmware for a hypothetical wearable device that will
vibrate when the wearer is facing North. The idea of that is to see of that
sensory input becomes naturally interpreted by your subconscious giving you an
inherent ability to sense North.

## Setting up your build environment ##
To build this project you will need the appropriate AVR toolchain for your
operating system.

### MacOS ###
1. Ensure that [Homebrew](https://brew.sh/) is installed
1. Tap the avr keg `brew tap osx-cross/avr`
1. Install the toolchain `brew install avr-libc`

### Linux ###
1. Install the toolchain `sudo apt-get install gcc-avr binutils-avr avr-libc`

### Windows ###
1. Download and install [git-bash](https://git-scm.com/download/win)
1. Download and install [WinAVR](http://winavr.sourceforge.net/download.html)

## Building ##
The Makefile is set up for a board with an ATMega32u4 processor running at
16MHz. If you are using a different processor or a difference clock frequency,
you will need to update the Makefile with the correct information. Once that is
done, run the `make` command in the project directory. If you have the toolchain
set up correctly this will produce a file called `dwmys.hex` that you can flash
to your device.
