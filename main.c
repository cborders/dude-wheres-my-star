#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdint.h>
#include <stdio.h>
#include <util/delay.h>

#include "BNO055.h"
#include "neo_pixel.h"
#include "uart.h"

#define CPU_PRESCALE(n)	(CLKPR = 0x80, CLKPR = (n))
#define CPU_16MHz 0
#define F_CPU 16000000UL

#define NUM_PIXELS 12

void fill_color(uint8_t* color) {
    for(uint8_t lights = 0; lights < NUM_PIXELS; lights++) {
        for(uint8_t i = 0; i <= lights; i++) {
            send(color);
        }
        show();
        _delay_ms(100);
    }
}

void solid_color(uint8_t* color) {
    for(uint8_t i = 0; i <= NUM_PIXELS; i++) {
        send(color);
    }
    show();
}

void light_degree(uint16_t degree, uint8_t* color) {
    uint16_t degreesPerLed = 360 / NUM_PIXELS;
    uint8_t targetLed = (uint8_t)(degree / degreesPerLed) % NUM_PIXELS;
    for(uint8_t i = 0; i <= NUM_PIXELS; i++) {
        if(i == targetLed) {
            send(color);
        } else {
            send(OFF);
        }
    }
    show();
}

void main(void) {
    CPU_PRESCALE(CPU_16MHz);

    init_neo_pixels();

    if(!init_sensor()) {
        while(1) {
            solid_color(RED);
            _delay_ms(500);
            solid_color(OFF);
            show();
            _delay_ms(500);
        }
    }

    uint16_t degree = 0;
    while(1) {
        degree = read_heading();
        light_degree(360 - degree, RED);
        _delay_ms(100);
    }
}
