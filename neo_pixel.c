#include "neo_pixel.h"

#include <avr/io.h>
#include <util/delay.h>

uint8_t OFF[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
uint8_t WHITE[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
uint8_t RED[] = { 0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0 };
uint8_t GREEN[] = { 1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
uint8_t BLUE[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1 };

#define PIN PORTB0
#define DDR DDRB
#define PORT PORTB

// 0.3us
#define _T0H() do { __asm__ __volatile__ ("nop\nnop\nnop"); } while (0)
// 0.6us
#define _T1H() do { __asm__ __volatile__ ("nop\nnop\nnop\nnop\nnop\nnop"); } while (0)
// 0.9us
#define _T0L() do { __asm__ __volatile__ ("nop\nnop\nnop\nnop\nnop\nnop\nnop"); } while (0)
// 0.6us
#define _T1L() do { __asm__ __volatile__ ("nop\nnop\nnop"); } while (0)
// Not super critial as long as it's long enough
#define TRESET 80

void init_neo_pixels() {
    DDR |= (1 << PIN);
}

inline void _ONE() {
    PORT |= (1 << PIN);
    _T1H();
    PORT &= ~(1 << PIN);
    _T1L();
}

inline void _ZERO() {
    PORT |= (1 << PIN);
    _T0H();
    PORT &= ~(1 << PIN);
    _T0L();
}

// Unrolled the loop here because the timing is critical
void send(uint8_t* color_data) {
    if(color_data[0] == 1) _ONE(); else _ZERO();
    if(color_data[1] == 1) _ONE(); else _ZERO();
    if(color_data[2] == 1) _ONE(); else _ZERO();
    if(color_data[3] == 1) _ONE(); else _ZERO();
    if(color_data[4] == 1) _ONE(); else _ZERO();
    if(color_data[5] == 1) _ONE(); else _ZERO();
    if(color_data[6] == 1) _ONE(); else _ZERO();
    if(color_data[7] == 1) _ONE(); else _ZERO();
    if(color_data[8] == 1) _ONE(); else _ZERO();
    if(color_data[9] == 1) _ONE(); else _ZERO();
    if(color_data[10] == 1) _ONE(); else _ZERO();
    if(color_data[11] == 1) _ONE(); else _ZERO();
    if(color_data[12] == 1) _ONE(); else _ZERO();
    if(color_data[13] == 1) _ONE(); else _ZERO();
    if(color_data[14] == 1) _ONE(); else _ZERO();
    if(color_data[15] == 1) _ONE(); else _ZERO();
    if(color_data[16] == 1) _ONE(); else _ZERO();
    if(color_data[17] == 1) _ONE(); else _ZERO();
    if(color_data[18] == 1) _ONE(); else _ZERO();
    if(color_data[19] == 1) _ONE(); else _ZERO();
    if(color_data[20] == 1) _ONE(); else _ZERO();
    if(color_data[21] == 1) _ONE(); else _ZERO();
    if(color_data[22] == 1) _ONE(); else _ZERO();
    if(color_data[23] == 1) _ONE(); else _ZERO();
}

void show() {
    PORT &= ~(1 << PIN);
    _delay_us(TRESET);
}
