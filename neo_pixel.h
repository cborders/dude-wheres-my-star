#ifndef _NEO_PIXEL_H_
#define _NEO_PIXEL_H_

#include <stdint.h>

extern uint8_t OFF[];
extern uint8_t WHITE[];
extern uint8_t RED[];
extern uint8_t GREEN[];
extern uint8_t BLUE[];

void init_neo_pixels(void);
void send(uint8_t* color_data);
void show(void);

#endif // _NEO_PIXEL_H_
