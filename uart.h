#ifndef _UART_H_
#define _UART_H_

#include "usb_serial.h"

void init_uart(void);
void send_str(const char *s);
uint8_t recv_str(char *buf, uint8_t size);

#endif // _UART_H_
